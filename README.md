# Groundstation Deploy

## Install

1. Setup nvidia-docker:
   On ubuntu 20.04, you can just run:

```bash
install/install_docker.ubuntu.sh
```

2. Setup cache directory:

```bash
install/init.sh
```

## Configuration
Groundstation is configured using environment variables in `docker-compose.yml`.

### Environment Variables
#### MAVLink Configuration Variables

The following environment variables can be used to configure MAVLink communication:

- `GS_MAVLINK_ADDRESSES`: Specifies the MAVLink connection endpoints. Format is `protocol:host:port` or `device_path`.
   - Example: `tcp:10.10.0.106:5760` for TCP connection
   - Can include multiple addresses separated by commas, e.g., `tcp:0.0.0.0:5760,udp:0.0.0.0:14550`
   - For serial connections, can specify device path like `/dev/ttyUSB0` and optionally baud rate like `/dev/ttyUSB0@57600`

- `GS_MAVLINK_GROUNDSTATION_SYS_ID`: Sets the system ID for the ground station in the MAVLink network.
   - Default: `255`
   - Range: 1-255
   - Ground stations typically use ID 255

- `GS_MAVLINK_MANUAL_CONTROL_METHOD`: Defines how manual control commands are sent to the vehicle.
   - Options:
      - `goto`: Sends GPS move commands (safe but less precise)
      - `manual_control`: Uses virtual sticks (may conflict with RC safety control)
      - `attitude`: Sends attitude commands
   - Default: `goto`

- `GS_MAVLINK_DEFAULT_BAUDRATE`: Sets the default baud rate for serial connections.
   - Default: `57600`
   - Common values: 57600, 115200
   - Only applies to serial connections (e.g., USB/UART devices)

Note if you want to use `tcpin`, you need to map port from host to container in `docker-compose.yml`:
e.g. to open port 5760 on host, add the following to `docker-compose.yml`:
```yaml
services:
  groundstation:
    ports:
      - "5760:5760"
```
  

## Run

Start the groundstation:

```bash
docker compose up
```

Stop the groundstation:

```bash
docker compose down
```